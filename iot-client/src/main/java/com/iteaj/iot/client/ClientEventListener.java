package com.iteaj.iot.client;

import com.iteaj.iot.event.IotEvent;
import com.iteaj.iot.event.StatusEventListener;

public interface ClientEventListener extends StatusEventListener<IotClient, ClientComponent> {

    @Override
    default boolean isMatcher(IotEvent event) {
        return StatusEventListener.super.isMatcher(event)
                && event.getComponent() instanceof ClientComponent;
    }
}
