package com.iteaj.iot.tools.db.rdbms;

import com.iteaj.iot.tools.db.FieldMeta;
import com.iteaj.iot.tools.db.IdType;
import com.iteaj.iot.tools.db.sql.SqlAnnotationMeta;

import java.util.List;

public class RdbmsMeta extends SqlAnnotationMeta {

    /**
     * 包含有注解IotTable的类
     * @see com.iteaj.iot.tools.annotation.IotTable
     * @param entityClass
     */
    public RdbmsMeta(Class<?> entityClass) {
        super(entityClass);
    }

    /**
     * 自定义
     * @param tableName
     * @param fieldMetas
     */
    public RdbmsMeta(String tableName, List<FieldMeta> fieldMetas) {
        super(tableName, fieldMetas);
    }
}
