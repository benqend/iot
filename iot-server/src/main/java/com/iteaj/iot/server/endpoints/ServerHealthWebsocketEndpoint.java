package com.iteaj.iot.server.endpoints;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.iteaj.iot.server.websocket.WebSocketServerListener;
import com.iteaj.iot.server.websocket.impl.DefaultWebSocketServerProtocol;

import java.util.Optional;

/**
 * 应用服务详情信息
 */
public class ServerHealthWebsocketEndpoint implements WebSocketServerListener {

    private long startupDate;

    public ServerHealthWebsocketEndpoint(long startupDate) {
        this.startupDate = startupDate;
    }

    @Override
    public String uri() {
        return "/ws/endpoint/health";
    }

    @Override
    public void onText(DefaultWebSocketServerProtocol protocol) {
        String requestText = protocol.readText();
        Optional<String> type = protocol.requestMessage().getQueryParam("type");
        if(StrUtil.isNotBlank(requestText) && !type.isPresent()) {
            try {
                type = Optional.ofNullable(JSON.parseObject(requestText).getString("type"));
            } catch (Exception e) {
                protocol.response(Result.fail("请求参数只支持Json格式").textJson());
                return;
            }
        }

        if(type.isPresent()) {
            Result info;
            switch (type.get()) {
                case "jvm":
                    info = Result.success(ServerHealthBuilder.buildJvmInfo());
                    break;
                case "system":
                    info = Result.success(ServerHealthBuilder.buildSystemInfo());
                    break;
                case "server":
                    info = Result.success(ServerHealthBuilder.buildComponentInfo());
                    break;
                default: info = Result.fail("错误的参数值["+type+"], 可选值[jvm, system, server]");
            }

            protocol.response(info.textJson());
        } else {
            Result result = ServerHealthBuilder.toResult(startupDate);
            protocol.response(result.textJson());
        }
    }

    @Override
    public void onClose(DefaultWebSocketServerProtocol protocol) {

    }

    @Override
    public void onBinary(DefaultWebSocketServerProtocol protocol) {
        protocol.response(Result.fail("不支持使用二进制请求").binaryJson());
    }

}
