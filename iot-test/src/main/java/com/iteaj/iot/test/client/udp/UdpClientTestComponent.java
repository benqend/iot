package com.iteaj.iot.test.client.udp;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.component.UdpClientComponent;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(prefix = "iot.test", name = "udp.start", havingValue = "true")
public class UdpClientTestComponent extends UdpClientComponent<UdpClientTestMessage> {

    @Override
    protected ServerInitiativeProtocol<UdpClientTestMessage> doGetProtocol(UdpClientTestMessage message, ProtocolType type) {
        return new UdpClientServerInitProtocol(message);
    }

    @Override
    public String getName() {
        return "Udp Client Test";
    }

    @Override
    public String getDesc() {
        return "用于Udp客户端测试";
    }
}
