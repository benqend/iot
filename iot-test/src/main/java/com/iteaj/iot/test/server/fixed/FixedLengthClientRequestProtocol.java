package com.iteaj.iot.test.server.fixed;

import cn.hutool.core.util.RandomUtil;
import com.iteaj.iot.ChannelManager;
import com.iteaj.iot.FrameworkManager;
import com.iteaj.iot.Message;
import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.server.manager.TcpDeviceManager;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.test.MessageCreator;
import com.iteaj.iot.test.TestProtocolType;

import java.util.concurrent.TimeUnit;

public class FixedLengthClientRequestProtocol extends ClientInitiativeProtocol<FixedLengthServerMessage> {

    public FixedLengthClientRequestProtocol(FixedLengthServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected FixedLengthServerMessage doBuildResponseMessage() {
        Message.MessageHead head = requestMessage().getHead();
        String equipCode = head.getEquipCode();
        String messageId = head.getMessageId();
        return MessageCreator.buildFixedLengthServerMessage(equipCode, messageId, FrameworkManager.getUseTcp(), head.getType());
    }

    @Override
    protected void doBuildRequestMessage(FixedLengthServerMessage requestMessage) {
        int timeout = RandomUtil.randomInt(1, 3);
        try {
            // 用于测试服务端超时
            TimeUnit.SECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}
