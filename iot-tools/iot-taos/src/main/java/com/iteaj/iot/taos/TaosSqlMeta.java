package com.iteaj.iot.taos;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.iteaj.iot.tools.annotation.*;
import com.iteaj.iot.tools.db.FieldMeta;
import com.iteaj.iot.tools.db.ParamValue;
import com.iteaj.iot.tools.db.sql.SqlAnnotationMeta;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class TaosSqlMeta extends SqlAnnotationMeta {

    /**
     * 是否使用超级表自动创建数据表
     */
    boolean using;

    private STable sTable;

    /**
     * 数据表表名 支持使用SpEL表达式
     * @see STable#table()
     */
    private String dataTableName;

    /**
     * 超级表表名
     */
    private String sTableName;

    private String insertSql;

    private String paramSql;

    private String tagInsertSql;

    private String tagParamSql;

    private List<IotTagMeta> tags = new ArrayList<>();
    private Map<String, Field> fieldMap = new HashMap<>(16);

    private static ParamValue NULL = new ParamValue(null, null);

    /**
     * 使用注解生成元数据
     * @see STable
     * @param entityClass
     */
    protected TaosSqlMeta(Class<?> entityClass) {
        super(entityClass);
    }

    /**
     * 自定义元数据 自动创建数据表
     * @param stableName 超级表表名
     * @param dataTableName 支持使用SpEL表达式
     * @param fieldMetas
     */
    public TaosSqlMeta(String stableName, String dataTableName, List<FieldMeta> fieldMetas) {
        this(stableName, dataTableName, fieldMetas, true);
    }

    /**
     * 自定义元数据 自动创建数据表
     * @param using 是否自动创建数据表
     * @param stableName 超级表表名
     * @param dataTableName 支持使用占位符
     * @param fieldMetas
     */
    public TaosSqlMeta(String stableName, String dataTableName, List<FieldMeta> fieldMetas, boolean using) {
        this(stableName, dataTableName, fieldMetas, null);
        this.using = using;
    }

    /**
     * 自定义元数据 自动创建数据表
     * @param stableName 超级表表名
     * @param dataTableName 支持使用SpEL表达式
     * @param fieldMetas
     * @param tagMetas
     */
    public TaosSqlMeta(String stableName, String dataTableName, List<FieldMeta> fieldMetas, List<IotTagMeta> tagMetas) {
        super(stableName, fieldMetas);
        this.using = true;
        this.tags = tagMetas;
        this.sTableName = stableName;
        this.dataTableName = dataTableName;
        this.handleSqlStatement();
    }

    @Override
    protected void resolveEntityTypeToMetas(Class<?> entityClass) { }

    @Override
    protected void resolveInsertSql() { }

    protected TaosSqlMeta build() {
        this.sTable = getEntityClass().getAnnotation(STable.class);
        if(this.sTable == null) {
            throw new TaosException("实体类[" + this.getEntityClass().getSimpleName() + "]未找到注解[STable]");
        }

        this.using = this.sTable.using();
        this.sTableName = this.sTable.value();
        this.dataTableName = this.sTable.table();

        if(StrUtil.isBlank(this.dataTableName)) {
            throw new TaosException("数据表名必填[STable#table()]");
        }

        Field[] declaredFields = getEntityClass().getDeclaredFields();
        for(Field item : declaredFields) {
            if(!item.isAccessible()) {
                item.setAccessible(true);
            }

            fieldMap.put(item.getName(), item);

            IotTableId fieldId = item.getAnnotation(IotTableId.class);
            if(fieldId != null) {
                if(!Date.class.isAssignableFrom(item.getType()) && Long.class != item.getType() && long.class != item.getType()) {
                    throw new TaosException("Id注解字段["+item.getName()+"]必须是类型[Date or Long]");
                }

                this.getFieldMetas().add(0, new IotTableIdMeta(fieldId, item));
            }

            IotField field = item.getAnnotation(IotField.class);
            if(field != null) {
                this.getFieldMetas().add(new IotFieldMeta(field, item));
            }

            IotTag iotTag = item.getAnnotation(IotTag.class);
            if(iotTag != null) {
                tags.add(new IotTagMeta(iotTag, item));
            }
        }

        return this.handleSqlStatement();
    }

    protected TaosSqlMeta handleSqlStatement() {
        this.paramSql = "("+this.getFieldMetas().stream().map(item -> "?").collect(Collectors.joining(","))+")";
        this.insertSql = "(" + this.getFieldMetas().stream().map(item -> item.getName()).collect(Collectors.joining(","))+")";

        if(this.isUsing()) {
            if(StrUtil.isBlank(this.sTableName)) {
                throw new TaosException("使用超级表自动创建数据表时超级表的表名必填");
            }

            if(CollectionUtil.isNotEmpty(this.tags)) {
                this.tagInsertSql = "USING " + sTable.value() + " (" + tags.stream().map(item -> item.getName()).collect(Collectors.joining(",")) + ")";
                this.tagParamSql = "(" + this.tags.stream().map(item -> "?").collect(Collectors.joining(",")) + ")";
            }
        }

        return this;
    }

    /**
     * 超级表表名
     * @return
     */
    @Override
    public String getTableName() {
        return this.sTableName;
    }

    public List<ParamValue> getTagParams(Object value) {
        List<ParamValue> params = new ArrayList<>();
        this.tags.forEach(item -> {
            Object fieldValue = ReflectUtil.getFieldValue(value, item.getField());
            params.add(new ParamValue(item, fieldValue));
        });

        return params;
    }

    public String getFieldValue(String fieldName, Object entity) {
        Field field = this.fieldMap.get(fieldName);
        if(field != null) {
            Object fieldValue = ReflectUtil.getFieldValue(entity, field);
            if(fieldValue != null) {
                return fieldValue.toString();
            }
        }

        return null;
    }

    public String getInsertSql() {
        return insertSql;
    }

    public String getParamSql() {
        return paramSql;
    }

    public STable getsTable() {
        return sTable;
    }

    public List<IotTagMeta> getTags() {
        return tags;
    }

    public String getTagInsertSql() {
        return tagInsertSql;
    }

    public void setTagInsertSql(String tagInsertSql) {
        this.tagInsertSql = tagInsertSql;
    }

    public String getTagParamSql() {
        return tagParamSql;
    }

    public void setTagParamSql(String tagParamSql) {
        this.tagParamSql = tagParamSql;
    }

    public String getDataTableName() {
        return dataTableName;
    }

    public boolean isUsing() {
        return using;
    }
}
